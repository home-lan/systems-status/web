# What is this?

A systems status page intended for use on a private LAN (such as a home network)

# Planned development:

## v1.0.0:

- [ ] Vanilla HTML table
- [ ] Uptime
- [ ] RAM utilization
- [ ] CPU utilization
- [ ] Storage utilization
- [ ] Temperature
- [ ] Server app to retrieve these pieces
- [ ] Credits
- [ ] AGPL notice
- [ ] App version number
- [ ] Dependency version number


## v1.0.1:

- [ ] Code cleanup / organization


 ## v2.0.0:

- [ ] Modern web app UI
- [ ] Responsive design
- [ ] Move data aggregation into server side app


## v2.0.1:

- [ ] Externalize server-side app into separate repo. This then becomes purely a JS / HTML front end.


## v2.1.0:

- [ ] Installable PWA
- [ ] Offline support


## v2.2.0:

- [ ] App icon for each hosted app
- [ ] Hosted app version number
- [ ] Red / green


## v2.3.0:

- [ ] Logs
- [ ] Reboot
- [ ] Shutdown
- [ ] Wipe (admin password required)
- [ ] Manually kick off backup (for all, or individually)


## v2.4.0:

- [ ] Live updating
